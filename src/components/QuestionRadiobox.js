import React from "react";

const QuestionRadiobox = ({ index, onChoose }) => {
  return (
    <>
      <div className="question-radiobox">
        <div className="radio-txt radio-disagree">Disagree</div>
        <input
          type="radio"
          className="radio"
          name={index}
          onClick={() => onChoose(-2, index)}
        />
        <input
          type="radio"
          className="radio"
          name={index}
          onClick={() => onChoose(-1, index)}
        />
        <input
          type="radio"
          className="radio"
          name={index}
          onClick={() => onChoose(0, index)}
        />
        <input
          type="radio"
          className="radio"
          name={index}
          onClick={() => onChoose(1, index)}
          // disabled={!isActive && "disabled"}
        />
        <input
          type="radio"
          className="radio"
          name={index}
          onClick={() => onChoose(2, index)}
          // disabled={!isActive && "disabled"}
        />
        <div className="radio-txt radio-agree">Agree</div>
      </div>
      <div className="radio-txt-mobile radio-disagree">Disagree</div>
      <div className="radio-txt-mobile radio-agree">Agree</div>
    </>
  );
};

export default QuestionRadiobox;
