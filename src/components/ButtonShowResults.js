import React from "react";
import { useNavigate } from "react-router";

const ButtonShowResults = ({ active }) => {
  let navigate = useNavigate();
  return (
    <button
      className={!active ? "btn btn-inactive" : "btn"}
      onClick={() => navigate("./results")}
      disabled={!active}
    >
      Show results
    </button>
  );
};

export default ButtonShowResults;
