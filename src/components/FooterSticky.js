import React from "react";
import FooterProgressBar from "./FooterProgressBar";

const FooterSticky = ({  progress = 0, counter }) => {


  return (
    <footer className="footersticky">
      <FooterProgressBar progress={progress} counter={counter} />
    </footer>
  );
};

export default FooterSticky;
