import React from "react";
import styled from "styled-components";
import { totalsPercentage } from "../utils/scores";
import { Colors } from "../utils/colors";

const colorObj = {
  1: Colors.pinkRGB,
  2: Colors.yellowRGB,
  3: Colors.orangeRGB,
  4: Colors.redRGB,
  5: Colors.darkgreenRGB,
  6: Colors.greenRGB,
  7: Colors.limeRGB,
  8: Colors.purpleRGB,
  9: Colors.magentaRGB,
};

const Ul = styled.ul`
  padding: 0;
`;
const Li = styled.li`
  display: block;
  height: 30px;
  margin-bottom: 10px;
  line-height: 30px;
`;
const Enumber = styled.div`
  width: 30px;
  display: inline-block;
  font-weight: bold;
`;
const Ebar = styled.div`
  display: inline-block;
  min-width: 20px;
  width: calc(100% - 90px);
  height: 10px;
  background-color: lavender;
  text-align: left;
  position: relative;
  :after {
    content: "";
    display: inline-block;
    width: ${(props) => (props.percent > 1 ? `${props.percent}%` : `0%`)};
    background-color: ${(props) => `rgb(${colorObj[props.index + 1]})`};
    background: linear-gradient(
      90deg,
      ${(props) => `rgba(${colorObj[props.index + 1]}, .5)`} 0%,
      ${(props) => `rgba(${colorObj[props.index + 1]}, 1)`} 100%
    );
    transition: width 2s;
    height: 100%;
    left: 0;
    color: orange;
    bottom: 0;
    position: absolute;
    z-index: 1;
  }
  :before {
    content: "";
    display: inline-block;
    width: ${(props) => (props.percent > 1 ? `${props.percent}%` : `0%`)};
    background-color: #444b5d;
    transition: width 2s;
    height: 100%;
    left: 0;
    bottom: 0;
    position: absolute;
    z-index: 0;
  }
`;
const Epercent = styled.div`
  width: 60px;
  font-weight: bold;
  font-size: 0.8em;
  display: inline-block;
  :before {
    content: "${(props) => (props.percent > 5 ? `${props.percent}%` : ``)}";
  }
`;
const Graph = () => {
  return (
    <Ul>
      {Object.keys(totalsPercentage).map((key, index) => {
        if (totalsPercentage[key] > 0) {
          return (
            <Li key={key}>
              <Enumber>{index + 1}</Enumber>
              <Ebar percent={totalsPercentage[key]} index={index} />
              <Epercent percent={totalsPercentage[key]} />
            </Li>
          );
        }
        return false;
      })}
    </Ul>
  );
};

export default Graph;
