import "./App.css";
import { useState } from "react";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import { questionsFull, questionsMedium } from "./utils/questions";
import HeaderSticky from "./components/HeaderSticky";
import ScrollTop from "./components/ScrollTop";
import FooterSticky from "./components/FooterSticky";
import { toPercentage, setTotals } from "./utils/scores";
import Form from "./views/Form";
import Results from "./views/Results";

const test = false;
const questionList = test ? questionsMedium : questionsFull;

const App = () => {
  const [qActive, setQActive] = useState(0);
  const [progressPercentage, setProgressPercentage] = useState();
  const [questionCounter, setQuestionCounter] = useState(
    `0/${questionList.length}`
  );
  const onChoose = (value, index) => {
    // update questionlist
    questionList[index].score = value;
    setTotals(questionList);

    // scrollintoview
    const remIndex = Object.values(questionList).findIndex((entry) => {
      return entry.score === null;
    });
    setQActive(remIndex);

    // get progress in percentage
    const answered = Object.keys(questionList).reduce(
      (total, item) =>
        questionList[item].score !== null
          ? Number(total) + Number(1)
          : Number(total),
      0
    );
    setProgressPercentage(toPercentage(questionList.length, answered));
    // update counter
    setQuestionCounter(`${answered}/${questionList.length}`);
  };

  return (
    <>
      <HeaderSticky />
      <ScrollTop />

      <BrowserRouter>
        <Routes>
          <Route
            exact
            path="/"
            element={
              <>
                <Form
                  questions={questionList}
                  onChoose={onChoose}
                  qActive={qActive}
                />
                <FooterSticky
                  progress={progressPercentage}
                  counter={questionCounter}
                />
              </>
            }
          ></Route>
          <Route path="/results" element={<Results />}></Route>
        </Routes>
      </BrowserRouter>
    </>
  );
};

export default App;
