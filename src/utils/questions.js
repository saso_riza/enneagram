import jsonQuestions from "./questions.json";

const qTypes = Object.keys(jsonQuestions);

// full list of questions
const qFull = [];
qTypes.map((qType) =>
  jsonQuestions[qType].map((q) => {
    const obj = {};
    obj.question = q;
    obj.type = qType;
    obj.score = null;
    return qFull.push(obj);
  })
);

// medium and short list of questions
const qMedium = [];
qTypes.map((qType) =>
  jsonQuestions[qType].map((q, index) => {
    const obj = {};
    obj.question = q;
    obj.type = qType;
    obj.score = null;
    return index <= 0 && qMedium.push(obj);
  })
);
// shuffle each question array in the json
const shuffleArray = (arr) => arr.sort(() => Math.random() - 0.5);

shuffleArray(qFull);
shuffleArray(qMedium);

export const questionsFull = qFull;
export const questionsMedium = qMedium;
