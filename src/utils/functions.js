export const replaceAtIndex = (_string, _index, _newValue) => {
  if (_index > _string.length - 1) {
    return _string;
  } else {
    return (
      _string.substring(0, _index) + _newValue + _string.substring(_index + 1)
    );
  }
};
